FROM ruby:2.1

RUN apt-get update
RUN apt-get install nginx-full nodejs --assume-yes
RUN apt-get install libmysqlclient-dev mysql-client postgresql-client sqlite3 freetds-bin freetds-common freetds-dev --assume-yes

ADD nginx.conf /etc/nginx/sites-available/default
RUN echo "daemon off;" >> /etc/nginx/nginx.conf

RUN mkdir -p /usr/src/app/tmp/sockets
RUN mkdir -p /usr/src/app/tmp/pids
RUN mkdir -p /usr/src/app/log
RUN mkdir -p /usr/src/app/config
ADD unicorn.rb /usr/src/app/config/unicorn.rb

WORKDIR /tmp
ONBUILD ADD Gemfile Gemfile
ONBUILD ADD Gemfile.lock Gemfile.lock
ONBUILD RUN bundle install --without development test

WORKDIR /usr/src/app

ENV RAILS_ENV=production
ONBUILD ADD . /usr/src/app
ONBUILD RUN ["bundle", "exec", "rake assets:precompile"]

EXPOSE 80
CMD bundle exec unicorn -c config/unicorn.rb -D && nginx

